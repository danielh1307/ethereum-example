# build-geth
This section describes how to build a new version of [go-ethereum](https://github.com/ethereum/go-ethereum) in an Ubuntu based docker image.

## Table of Contents
* [Start Container and prepare build](#start-container-and-prepare-build)
* [Install Go](#install-go)
* [Clone repository and build go-ethereum](#clone-repository-and-build-go-ethereum)

## Start Container and prepare build
First, start the container and exec into it:
```
$ docker run -d -it ubuntu /bin/bash
```
Next, you have to install some packages to perform the build:
```bash
$ apt-get update
$ apt-get install git
$ apt-get install build-essential
$ apt-get install wget
```
## Install Go
Next, you have to install [Go](https://golang.org) to compile [go-ethereum](https://github.com/ethereum/go-ethereum)
```bash
$ cd /usr/local
$ wget https://storage.googleapis.com/golang/go1.9.2.linux-amd64.tar.gz
$ tar -xvf go1.9.2.linux-amd64.tar.gz
$ rm go1.9.2.linux-amd64.tar.gz
$ ln -s /opt/go/bin/go /usr/bin/go
```
## Clone repository and build go-ethereum
Next, you clone the repository of go-ethereum, choose the release you want (here we take 1.7) and build it:
```bash
$ mkdir /opt/geth
$ cd /opt/geth
$ git clone https://github.com/ethereum/go-ethereum
$ git checkout release/1.7
$ make all
```
In the end, the binary files are located at 
```
/opt/get/go-ethereum/build/bin
```