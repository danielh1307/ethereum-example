# docker
This directory contains useful docker images and everything you need to get your private chain running.
## Table of Contents
* [Container Overview](#container-overview)
* [Run the private chain and start mining](#run-the-private-chain-and-start-mining)
* [Connect from the host to the blockchain via JSON RPC](#connect-from-the-host-to-the-blockchain-via-json-rpc)
* [Basic use cases](#basic-use-cases)

## Container Overview
* [build-geth](build-geth): Contains an image to build [go-ethereum](https://github.com/ethereum/go-ethereum). You only need 
this if you want to build your own version of geth. 
* [eth-node](eth-node): Contains an image with geth and bootnode and a genesis block; that is basically everything you need to get your
private chain running.

## Run the private chain and start mining
To run the private chain, you can execute `docker-compose` with the `docker-compose.yml` provided here. Ìt starts five containers:
* bootnode: This can be used as a bootstrap node for other Ethereum member nodes. [See details here](https://github.com/ethereum/go-ethereum#creating-the-rendezvous-point).
* eth-node-1: First Ethereum member node, intended to run the blockchain. This container also forwards port 8545 to the host, so you can access the 
REST API of your blockchain.
* eth-node-2: Second Ethereum member node, can be used to add a second node to the blockchain (optionally, if you want to demonstrate mining from a second node).
* eth-client-1: This container is intended to connect to one of the member node's JavaScript runtime environment via the geth JavaScript console (REPL).
* eth-client-2: A second client for a JavaScript console to demonstrate some use cases.
* node: A container running node, to demonstrate the [web3.js](https://github.com/ethereum/web3.js/) library.  

To run the private chain, you can start up all these container:
```bash
$ docker-compose up -d
```
or just the minimum, one node and one client:
```bash
$ docker-compose up -d eth-node-1 eth-client-1
```
Afterwards, you connecet to eth-node-1 and start the blockchain:
```bash
$ docker exec -it eth-node-1 /bin/bash
$ cd /opt/geth
$ ./start-geth
```
If you want to connect this node to a bootnode, you have to start it with the enode of the bootnode 
(be aware to use the correct IP address, and, of course, you have to start the bootnode container):
```bash
$ ./start-geth enode://0ccbd143197a6eaf27637ae373dca84a38a02eac7e15ff1ae3d7197017954a42fbc8a9329d62154d52cd7010cc9e18d2fa41838ffa649aa0c21ba34c76fd9cc4@172.18.0.2:30301
```
Now you can open a JavaScript console from one of the clients:
```bash
$ docker exec -it eth-client-1 /bin/bash
$ geth attach http://eth-node-1:8545
Welcome to the Geth JavaScript console!

instance: Geth/v1.7.3-stable-4bb3c89d/linux-amd64/go1.9.2
 modules: admin:1.0 eth:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 web3:1.0
> 
```
That's it. Now you can create an account and start mining:
```bash
> myaccount = personal.newAccount("password");
> miner.setEtherbase(myaccount);
> miner.start()
```
## Connect from the host to the blockchain via JSON RPC
eth-node-1 forwards port 8545 to the host, so you can use this port to access the blockchain:
```
$ curl -d '{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["latest",true],"id":1}' -H "Content-Type:application/json" -X POST http://localhost:8545
```
See a full documentation of JSON RPC [here](https://github.com/ethereum/wiki/wiki/JSON-RPC).

## Basic use cases

### Transfer ether from one account to another
Let's assume you have two accounts: `first_account` represents your main account, `second_account` is a another account. 
You can check your balance on `first_account`:
```bash
> eth.getBalance(first_account)
```
If you want to transfer ether between `first_account` and `second_account`, you have to send a transaction and pass the amount you want to transfer in the `value` field:
```bash
> eth.sendTransaction({from:first_account, to:second_account, value: web3.toWei(1, "ether")})
```
The `value` in sendTransaction is passed in wei (the smallest unit for ether; 1 ether are 1.000.000.000.000.000.000 wei). So if you want to send 1 ether, you can convert it to wei with `web3.toWei()`.  

The 32 bytes transaction hash is returned as string, and with this we can check the transaction:
```bash
> eth.getTransaction(<HEX>)
> eth.getTransactionReceipt(<HEX>)
```

### Connect multiple member nodes to peers
We connect eth-node-1 and eth-node-2 to peers (apart from using a bootnode). 
Let's assume eth-node-2 enode is `enode://7c1af97ad0369400662102d24ebdbfcea078559da0efbcf981df6c776397f5e9680cb868862abd6174535e8c7809cf0baba6d28404b2287a606be3fc53ff73ae` and
eth-node-2 IP address is `172.18.0.5`.
We use eth-node-2 as peer for eth-node-1 by connecting from eth-client-1 to eth-node-1 via JavaScript console and then:
```
> admin.addPeer("enode://7c1af97ad0369400662102d24ebdbfcea078559da0efbcf981df6c776397f5e9680cb868862abd6174535e8c7809cf0baba6d28404b2287a606be3fc53ff73ae@172.18.0.5:30303")
```
Check this:
```bash
> admin.peers
```
Now you have two member nodes for the same blockchain. Please be aware if you want to unlock an account on eth-node-2 which was created on eth-node-1, you have to copy the keyfile
from the node (`/opt/geth/data/keystore`).