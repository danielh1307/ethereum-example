#!/usr/bin/env bash

geth --datadir=/opt/geth/data init /opt/geth/genesis.json
if [ -z $1 ]
then
  geth --datadir=/opt/geth/data --rpc --rpcaddr 0.0.0.0 --rpcapi="db,eth,net,web3,personal,miner,admin";
else
  geth --datadir=/opt/geth/data --bootnodes=$1 --rpc --rpcaddr 0.0.0.0 --rpcapi="db,eth,net,web3,personal,miner,admin";
fi