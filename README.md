# ethereum-example
Here you can find a few basic examples

* how to start a private Ethereum blockchain
* how to develop and deploy smart contracts
* how to call these smart contracts via JSON RPC and JavaScript

## Start your own private chain
To get your own private Ethereum blockchain up and running in a docker container,
see the instructions on [this site](docker). It also explains how you can set up
your first account and start to mine some ether.

## Develop, compile and deploy smart contracts
To see a few basic smart contract examples (written in Solidity), compile them and
deploy them to your own blockchain, see instructions on [this site](src).

### HelloWorld
A simple HelloWorld contract with a few more methods to demonstrate basic concepts.
There is also a [small tutorial](src/01-HelloWorld) how to call smart contracts from
outside the blockchain.

### StatefulContract
[This example](src/02-StatefulContract) demonstrates how a contract which keeps its
own state can be developed and how you can change that state. It also shows the concept
of events and filters, which lets you know the results of (asynchronous) transactions.

### ContractWithBalance
[The third example](src/03-ContractWithBalance) explains how a contract can keep its
own Ether, how you can send Ether to a contract and how these ether can be sent from
the contract to another account.
