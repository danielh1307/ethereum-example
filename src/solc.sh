#!/usr/bin/env bash

docker run --rm -v $(pwd)/$(dirname $1):/src --entrypoint solc ethereum/solc:stable --bin -o /src --abi /src/$(basename $1)