const fs = require('fs');
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

/* create an instance of the HelloWorld contract */
const helloWorldAbiJson = fs.readFileSync('HelloWorld.abi');
const helloWorldAbi = JSON.parse(helloWorldAbiJson);
const helloWorldContract = web3.eth.contract(helloWorldAbi);
const contractAddress = process.argv[2];
const helloWorldInstance = helloWorldContract.at(contractAddress);

/* call the functions on the contract */
const hellWorldString = helloWorldInstance.sayHello();
console.log("Contract says: " + hellWorldString);

const helloToString = helloWorldInstance.sayHelloTo("Daniel");
console.log("Contract says to Daniel: " + helloToString);

const onePlusTwoResult = helloWorldInstance.add(1, 2);
console.log("Contract can also calclulate: " + onePlusTwoResult);