pragma solidity ^0.4.18;

contract HelloWorld {
    /* this runs when the contract is deployed */
    function HelloWorld() public {
    }

    /*
     * A simple function which just returns "Hello World"
     * public - part of contract interface, can be called internally or via messages
     * pure - function does not read from the contract state or modifies it
     */
    function sayHello() public pure returns (string) {
        return "Hello World";
    }

    /*
     * This function demonstrates string concatenation. Returns "Hello " + the given name.
     */
    function sayHelloTo(string name) public pure returns (string) {
        return concat("Hello ", name);
    }

    function add(uint256 a, uint256 b) public pure returns (uint256) {
        return a + b;
    }

    /*
     * Helper function for string concatenation.
     * internal - function can only be called from within the contract.
     */
    function concat(string _a, string _b) pure internal returns (string) {
        bytes memory _ba = bytes(_a);
        bytes memory _bb = bytes(_b);

        string memory resultString = new string(_ba.length + _bb.length);
        bytes memory resultBytes = bytes(resultString);

        uint k = 0;

        for (uint i = 0; i < _ba.length; i++) {
            resultBytes[k++] = _ba[i];
        }
        for (i = 0; i < _bb.length; i++) {
            resultBytes[k++] = _bb[i];
        }

        return string(resultBytes);
    }
}