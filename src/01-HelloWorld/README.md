# HelloWorld
Basically a simple Hello World program with some additional functions. It demonstrates how you can interact with smart contracts in the blockcahin.

This smart contract does not have any state, therefore you do not need to send transactions to it. You can just call it
(for a good explanation of the difference, have a look [here](https://ethereum.stackexchange.com/questions/765/what-is-the-difference-between-a-transaction-and-a-call)).    

Deploy it as described in the [upper directory](..).

## Table of Contents
* [Call smart contract via JavaScript console](#call-smart-contract-via-javascript-console)
* [Call smart contract via JSON RPC](#call-smart-contract-via-json-rpc)
* [Calling more complex methods via JSON RPC](#calling-more-complex-methods-via-json-rpc)
* [Call smart contract via node](#call-smart-contract-via-node)

## Call smart contract via JavaScript console
```bash
# copy content from HelloWorld.abi to JSON.parse('')
> var helloWorldInterface = ether.contract(JSON.parse(''));
# copy contract address
> var helloWorld = helloWorldInterface.at("0x");
> helloWorld.sayHello()
"Hello World"
> helloWorld.add(1, 2)
3
```
Be aware you can call the contract even if no blocks are actually mined - this is because you are not sending transactions here.
## Call smart contract via JSON RPC
POST request with Content-Type: application/json:
```JSON
{
    "jsonrpc": "2.0",
    "method": "eth_call",
    "params": [{
        "from": "0x03208de1e7c41cd9929615685cdf418a79c3ec78",
        "to": "0x4010b03d224bad350cf91c1b1666d30b47824a48",
        "data": "0xef5fb05b"
    }, "latest"],
    "id": 5148
}
```
* from: the account which calls the contract
* to: the address of the contract
* data: the function you are calling (sayHello()) including parameters (none in this case). You can get this value by calling web3.sha3("sayHello()") and take the first 4 bytes:

```bash
> web3.sha3("sayHello()")
"0xef5fb05bbc81b50769bf6c6e6f297aa4f4055ecc730b088789683d2026c63cfb"
```
The return value is 
```
0x0000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000000b48656c6c6f20576f726c64000000000000000000000000000000000000000000
```
which is the hex representation of the string "Hello World".

## Calling more complex methods via JSON RPC
You can find an in-depth description [here](https://github.com/ethereum/wiki/wiki/Ethereum-Contract-ABI) and a good explanation of how to pass arguments [here](https://ethereum.stackexchange.com/questions/10862/deploying-contract-with-constructor-arguments-via-geth-rpc). 
Let's make two more exmples.

### add(uint256, uint256)
It is exactly the same request as for `sayHello(), except the data part. So let's put it together.  
First, you need to get the first 4 bytes of the Keccak-256 SHA3 hash of the function signature:
```bash
> web3.sha3("add(uint256,uint256)")
"0x771602f7f25ce61b0d4f2430f7e4789bfd9e6e4029613fda01b7f2c89fbf44ad"
```
So this is
```
771602f7
```
Next, you have to send 32 Bytes for each parameter (its hex value). Let's take 7 and 13:
```
0000000000000000000000000000000000000000000000000000000000000007
000000000000000000000000000000000000000000000000000000000000000D
```
Put it all together, and you get:
```JSON
{
    "jsonrpc": "2.0",
    "method": "eth_call",
    "params": [{
        "from": "0x03208de1e7c41cd9929615685cdf418a79c3ec78",
        "to": "0x4010b03d224bad350cf91c1b1666d30b47824a48",
        "data": "0x771602f70000000000000000000000000000000000000000000000000000000000000007000000000000000000000000000000000000000000000000000000000000000D"
    }, "latest"],
    "id": 5148
}
```
which results in the hex representation of 20.
```
0x0000000000000000000000000000000000000000000000000000000000000014
```

### sayHelloTo(string)
First 4 bytes of the SHA3 hash of the function signature are
```
6c0ff768
```
The string is a dynamic parameter, so first you have to give a 32 byte offset from the start until the string begins.
In this case this is 32 bytes (just the offset), so the offset is
```
0000000000000000000000000000000000000000000000000000000000000020
```
Next, you have to pass the number of bytes of the UTF-8 encoded string, so if we want to send "Daniel", these
are 6 bytes:
```
0000000000000000000000000000000000000000000000000000000000000006
```
Next, we pass the actual value (again, in hex form), filled up to 32 bytes:
```
44616e69656c0000000000000000000000000000000000000000000000000000
```
This gives the following data part:
```
0x6c0ff7680000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000000644616e69656c0000000000000000000000000000000000000000000000000000
```
which returns the correct 96 bytes value:
```
0000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000000c48656c6c6f2044616e69656c0000000000000000000000000000000000000000"
```
Let's disassemble it. The first 32 bytes are the offset (and represents 32 bytes):
```
0000000000000000000000000000000000000000000000000000000000000020
```
The next 32 bytes are the length of the UTF-8 encoded string (which represents 12):
```
000000000000000000000000000000000000000000000000000000000000000c
```
The last 32 bytes is the string itself ("Hello Daniel"):
```
48656c6c6f2044616e69656c0000000000000000000000000000000000000000
```

## Call smart contract via node
To use the smart contract in a node.js file, see the sample file `execHelloWorld.js`. You have to install the web3 library, afterwards 
just execute it and pass the address of the smart contract:
```bash
$ npm install web3
$ node execHelloWorld.js 0x764712c23ff883d26918fafef161c8c45e295eff
```
If you do not have node installed locally, you can also use the node container as started by [docker-compose](../../docker).