#!/usr/bin/env bash


curl -d '{"jsonrpc": "2.0", "method": "eth_getTransactionReceipt", "params": ["'$1'"],"id": 5148}' -H "Content-Type:application/json" -X POST http://localhost:8545 | jq '.'