#!/usr/bin/env bash

account=$1
binFile=$2
data='0x'`cat $2`

curl -d '{"jsonrpc": "2.0", "method": "eth_sendTransaction", "params": [{"from": "'$account'", "gas": "0x76c00", "data": "'$data'"}],"id": 5148}' -H "Content-Type:application/json" -X POST http://localhost:8545 | jq '.'