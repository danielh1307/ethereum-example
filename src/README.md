# Sample applications
This section contains a few simple smart contracts to demonstrate some basic concepts.

## Table of Contents
* [Compile applications](compile-applications)
* [Deploy applications](deploy-applications)

## Compile applications
To compile a `*.sol` file, you can use the script `solc.sh` (for Unix) or `solc.bat` (for Windows). You have to execute it in the bash (Windows: cmd) and have [jq](https://stedolan.github.io/jq/) installed (only for Unix).  
For example, to compile the `HelloWorld.sol` file for Unix, just execute
```bash
$ ./solc.sh 01-HelloWorld/HelloWorld.sol
```
Afterwards, the `HelloWorld.abi` and `HelloWorld.bin` files are located in the same directory as the source file.
## Deploy applications
### JSON RPC
To deploy a smart contract on Ethereum, you have to [send a transaction](https://github.com/ethereum/wiki/wiki/JSON-RPC#eth_sendtransaction) with the binary data of the contract to the blockchain. 
This is what is done by the script `deploy.sh` (for Unix) or `deploy.bat` (for Windows).
```bash
$ ./deploy.sh 0x115d07832fa0c33ecf49065bb200c5eecd6da992 01-HelloWorld/HelloWorld.bin
```
You have to pass two arguments: 
* the address from which the contract is created
* the binary data file.  

It returns the 32 byte transaction hash. As soon as this transaction is contained in a block, you can get the receipt (only for Unix):
```bash
$ ./getTransactionReceipt.sh 0x9367a020e38932fd06de28e947276bcb0715207378d8f116224d520af99052eb
```
And this receipt contains the contract address.
### JavaScript console
To deploy the smart contract via the REPL based JavaScript console, you have to open a console and execute the following commands:
```bash
> myaccount = personal.newAccount("account");
> miner.setEhterbase(myaccount);
> miner.start(2)
# copy content from HelloWorld.abi to JSON.parse('')
> var helloWorldAbi = eth.contract(JSON.parse(''));
# copy content from HelloWorld.bin
> var helloWorldCode = '0x';
> var helloWorld = helloWorldAbi.new({from: myaccount, data: helloWorldCode, gas: 1000000}, function(e, contracct) {})
# wait until it is mined ... (see output of 'helloWorld')
```

In the [HelloWorld smart contract section](01-HelloWorld) you can find some further explanations 
(e.g. how to call the smart contract from the JavaScript console or via JSON RPC).