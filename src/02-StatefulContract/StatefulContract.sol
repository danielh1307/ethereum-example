pragma solidity ^0.4.18;

contract StatefulContract {
    /* (stateful) array which maps an address to an int
     * since this is public, the compiler automatically generates a getter for it
     */
    mapping (address => int) public callings;

    /* logging facility */
    event Log(
        address _address,
        int numberOfCalls,
        string _value
    );

    /* this runs when the contract is deployed */
    function StatefulContract() public {
    }

    /*
     * Function adds the number of calls from the sending address to its state.
     * external - function can only be called from outside the contract
     * no return value - since the state is changed, a block has to be mined to accomplish this change.
     *      So only a transaction hash is returned, and as soon as this transaction is in a mined block,
     *      the change is performed.
     */
    function call() external returns (int) {
        address callingAddress = msg.sender;
        int numberOfCalls = callings[callingAddress];
        numberOfCalls++;
        callings[callingAddress] = numberOfCalls;
        Log(callingAddress, numberOfCalls, "call() was called");
    }
}