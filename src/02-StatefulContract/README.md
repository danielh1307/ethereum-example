# StatefulContract
Demonstrates 
* how a contract can keep state
* how to send a transaction to a contract
* how to log the result of a transaction

## Sending a transaction to the contract
Deploy the contract as described in the [Deploy section](..).  

The contract has just one external method, `call()`, which keeps track how many times 
the contract has been called from a specific address.

You can either call the contract via the JavaScript REPL console or via JSON RPC. Since
the function changes the state of the contract, a new transaction is submitted and the
transaction hash is returned. You have to mine a new block which includes this transaction
to actually "see" the state change.

### Call the smart contract via JavaScript console
Let's assume the contract is assigned to `c` from the account `myaccount`. Just call it:
```
> c.call()
"0x5efd279953960144799a67cd6a0d16475375c8a353f0ce20ec02fc1efe7193dc"
```
The return value is the hash of the submitted transaction.  
If you want to check the result, you probably get the result 0 (since `callings` is a public variable, the solidity
compiler automatically generates a getter for it):
```
c.callings(myaccount)
0
```
This is because the block with your transaction has not been mined yet. As soon as the block
is mined, you get the correct result:
```
c.callings(myaccount)
1
```

### Call the smart contract via JSON RPC
This is straight-forward. You can optionally pass a gasPrice:
```JSON
{
    "jsonrpc": "2.0",
    "method": "eth_sendTransaction",
    "params": [{
        "from": "0xbc005b373188e9dd20535eebd00459f158b12f03",
        "to": "0x1908011299b170ce854be7f18f3a05f04111da58",
        "gasPrice": "0x9184e72a000",
        "data": "0x28b5e32b"
    }],
    "id": 5149
}
```
Again the transaction hash is returned:
```JSON
{
  "jsonrpc": "2.0",
  "id": 5149,
  "result": "0xbe83e67f7e9504fc128677a21ee228550e18195c15f077bda614f083f985586d"
}
```

## Filter
To get access to the Log event, you have to register a filter:
```JSON
{
    "jsonrpc": "2.0",
    "method": "eth_newFilter",
    "params": [{
    }],
    "id": 5149
}
```
The return value is an ID which can be used to get the filter events:
```JSON
{
    "jsonrpc": "2.0",
    "method": "eth_getFilterChanges",
    "params": [
        "0xac7e23c5c6f6e977ed55cced77812ee0"
    ],
    "id": 5149
}
```
You receive each event exactly once. The data part contains the content of the Log event:
```JSON
{
  "jsonrpc": "2.0",
  "id": 5149,
  "result": [
    {
      "address": "0x2dc83fbd6bd978a90d652f158b750ddf3fe8d6c0",
      "topics": [
        "0x2d343989bd55963a66f8c95620ab15d1be8782cc421278f0355cb88c6e31e77d"
      ],
      "data": "0x0000000000000000000000007ba6e62f3738da79673890f4c71da11f1940ce5800000000000000000000000000000000000000000000000000000000000000050000000000000000000000000000000000000000000000000000000000000060000000000000000000000000000000000000000000000000000000000000001163616c6c2829207761732063616c6c6564000000000000000000000000000000",
      "blockNumber": "0x5f9",
      "transactionHash": "0x1533c1b0cf33469ccd82891f72be7e0bfa836ad9421087fa2649b1ca23201a27",
      "transactionIndex": "0x0",
      "blockHash": "0x44f558ef4a64dc03881239bcc51c8f062fc854aa6bc34e546b2817245a7302a2",
      "logIndex": "0x0",
      "removed": false
    }
  ]
}
```
The Ethereum Wallet browser also lets you watch the contract events.

