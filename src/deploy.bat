SET ACCOUNT=%1
SET BINFILE=%2

for /f "delims=" %%x in (%2) do set DATA=%%x
SET DATA=0x%DATA%

curl -d "{\"jsonrpc\": \"2.0\", \"method\": \"eth_sendTransaction\", \"params\": [{\"from\": \"%ACCOUNT%\", \"gas\": \"0x76c00\", \"data\": \"%DATA%\"}],\"id\": 5148}" -H "Content-Type:application/json" -X POST http://localhost:8545