# Contract with balance
Demonstrates
* how to send ether to a smart contract
* how to send ether from a smart contract to an externally owned account

Sending ether to the contract:
```JSON
 { 
  from: '0xe0c2e5a90d419ef77f91ff6b38342a9d606976e4',
  to: '0x77e0ee3bfe4f1a964ef5b1858aba93c233065365',
  data: '0xa3912ec8',
  value: '0x4563918244f40000',
  gasPrice: '0xba43b7400' }
```
Sending ether from the contract to another account (call method `sendEther`):
```JSON
{
    "jsonrpc": "2.0",
    "method": "eth_sendTransaction", 
    "params": [ { 
        "from": "0xe0c2e5a90d419ef77f91ff6b38342a9d606976e4",
        "to": "0x1ddd3f3be9115102e79f0fe69428f571b7049417",
        "data": "0xc1756a2c0000000000000000000000000397f7e52329534343bcb92743622ea15e3f896b0000000000000000000000000000000000000000000000000000000000000001",
        "gasPrice": "0xba43b7400"
        }],
    "id": 5148
}
```