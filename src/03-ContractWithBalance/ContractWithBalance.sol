pragma solidity ^0.4.0;

contract ContractWithEther {
    function receiveEther() public payable { }

    function sendEther(address receiver, uint256 amount) public {
        receiver.transfer(amount);
    }
}